function getRnd(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}

let progressVal = 0;
const PROGRESS_MAX = 100;
setInterval(() => {
	if (progressVal === PROGRESS_MAX) {
		progressVal = 0;
	} else {
		progressVal += getRnd(1, 20);
		progressVal = Math.min(PROGRESS_MAX, progressVal);
	}

	document.querySelectorAll('round-progress').forEach((p) => {
		p.setAttribute('value', progressVal);
	});
}, 500);
