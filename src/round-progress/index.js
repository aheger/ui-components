const TEMPLATE = `
	<style>
		:host {
			display: inline-block;
			--progress-background: #ccc;
			--progress-transition: 0.2s ease-in;
			--progress-linecap: butt;
		}

		circle.foreground {
			transition: stroke-dashoffset var(--progress-transition);
			stroke-linecap: var(--progress-linecap);
		}

		circle.background {
			color: var(--progress-background);
		}
	</style>
	<svg>
		<circle
			class="background"
			stroke="currentColor"
			fill="transparent"
		/>
		<circle
			class="foreground"
			stroke="currentColor"
			fill="transparent"
			transform="rotate(-90)"
			transform-origin="center"
		/>
	</svg>
`;

class RoundProgress extends HTMLElement {
	static get observedAttributes() {
		return [
			'max',
			'value',
		];
	}

	constructor() {
		super();

		this.root = this.attachShadow({mode: 'closed'});
		this.root.innerHTML = TEMPLATE;
		this.svg = this.root.querySelector('svg');
		this.foreground = this.root.querySelector('circle.foreground');
		this.background = this.root.querySelector('circle.background');
		this.update();
	}

	attributeChangedCallback() {
		this.update();
	}

	update() {
		const value = Number(this.getAttribute('value'));
		if (isNaN(value)) {
			return;
		}
		const max = Number(this.getAttribute('max'));
		if (isNaN(max)) {
			return;
		}

		const radius = Number(this.getAttribute('radius') || 50);
		if (isNaN(radius)) {
			return;
		}
		const stroke = Number(this.getAttribute('stroke') || 4);
		if (isNaN(stroke)) {
			return;
		}

		const normalizedRadius = radius - 0.5 * stroke;
		const circumference = normalizedRadius * 2 * Math.PI;

		let strokeDashoffset;
		if (max === 0) {
			strokeDashoffset = circumference;
		} else {
			strokeDashoffset = circumference - (value / max) * circumference;
		}

		this.svg.setAttribute('height', radius * 2);
		this.svg.setAttribute('width', radius * 2);

		this.foreground.setAttribute('stroke-dasharray', circumference);
		this.foreground.setAttribute('stroke-dashoffset', strokeDashoffset);

		[this.foreground, this.background].forEach(el => {
			el.setAttribute('stroke-width', stroke);
			el.setAttribute('r', normalizedRadius);
			el.setAttribute('cx', radius);
			el.setAttribute('cy', radius);
		});
	}
}

customElements.define('round-progress', RoundProgress);
